from pygame.rect import Rect

from levels.snake_levels import SnakeLevels
from sprites.sprite_snake import Snake

# Sådan starter alle Pygames
import pygame

pygame.init()

# "spillet_kører" sætter vi til False, når spillet skal stoppe
spillet_kører = True

# Her laver vi de elementer der skal være med i spillet
snake_levels = SnakeLevels()
list_of_levels = snake_levels.levels
current_level = list_of_levels[0]


retning = current_level.snake.start_direction

# Her åbner vi en ny skærm
skærm_brede = 800
skærm_højde = 600
skærm_størrelse = (skærm_brede, skærm_højde)
skærm = pygame.display.set_mode(skærm_størrelse)
pygame.display.set_caption('Fedt navn')

# "ur" bestemmer hvor hurtigt vi skal opdatere skærmen
ur = pygame.time.Clock()

# Her er de metoder som bliver brugt i spillet
def opdater_spil_elementer():
    opdater_slange()
    tegn_baggrund()
    tegn_slange()
    tegn_mure()
    tegn_bananer()
    ramte_slangen_en_banan()
    #


def opdater_slange():
    current_level.snake.set_snake_direction(retning)


def tegn_baggrund():
    skærm.fill(current_level.background_color)


def tegn_slange():
    liste_af_koordinater = current_level.snake.get_rect_to_draw()
    for koordinat in liste_af_koordinater:
        pygame.draw.rect(skærm, current_level.snake.color, koordinat)


def tegn_mure():
    for wall in current_level.list_of_walls:
        pygame.draw.rect(skærm, wall.get_color(), wall.get_rect())


def tegn_bananer():
    current_level.list_of_bananas.draw(skærm)


def ramte_slangen_væggen():
    slange_hoved = current_level.snake.snake_joins[0]
    for wall in current_level.list_of_walls:
        if pygame.Rect.colliderect(Rect(wall.get_rect()), Rect(slange_hoved)):
            return True


def ramte_slangen_en_banan():
    slange_hoved = current_level.snake.snake_joins[0]

    for banan in current_level.list_of_bananas:
        rect1 = Rect(banan.rect)
        if pygame.Rect.colliderect(rect1, slange_hoved):
            current_level.snake.increase_snake_length()
            current_level.list_of_bananas.remove(banan)





# Her er vores program
# Det er en løkke, som kører indtil "spillet_kører" bliver sat til False
while spillet_kører:

    # 2. Her lytter vi efter input
    for event in pygame.event.get():
        if event.type == pygame.QUIT: #Spilleren lukker vinduet
            spillet_kører = False

    # Her lytter vi på keyboard input
    knapperPresset = pygame.key.get_pressed()
    if knapperPresset[pygame.K_a] and retning != Snake.HØJRE:
        retning = Snake.VENSTRE
    elif knapperPresset[pygame.K_d] and retning != Snake.VENSTRE:
        retning = Snake.HØJRE
    elif knapperPresset[pygame.K_w] and retning != Snake.NED:
        retning = Snake.OP
    elif knapperPresset[pygame.K_s] and retning != Snake.OP:
        retning = Snake.NED
    elif knapperPresset[pygame.K_SPACE]:
        current_level = list_of_levels[1] # Bare for at teste

    # Her opdaterer vi spillets variable
    opdater_spil_elementer()

    if ramte_slangen_væggen():
        spillet_kører = False


    # Her opdateres det hele
    pygame.display.flip()
    # 60 betyder at vi forsøger at tegne 60 gange i sekundet
    ur.tick(60)

pygame.quit()
quit()

