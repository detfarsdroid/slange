import pygame

from levels.colors import Colors
from sprites.sprite_banana import Banana
from sprites.sprite_snake import Snake
from sprites.sprite_wall import Wall


class Level:
    background_color = Colors.color_green

    def __init__(self, list_of_walls, list_of_bananas, snake_x, snake_y, snake):
        self.list_of_walls = list_of_walls
        self.list_of_bananas = list_of_bananas
        self.snake_start_x = snake_x
        self.snake_start_y = snake_y
        self.snake = snake

    def set_background_color(self, color):
        self.background_color = color


class SnakeLevels:
    levels = []

    def __init__(self):
        level1 = self.get_level_1()
        self.levels.append(level1)
        self.levels.append(self.get_level_2())

    def get_level_1(self):
        # Walls:
        list_of_walls = []
        list_of_walls.append(Wall(50, 50, 10, 40, Colors.color_white))
        list_of_walls.append(Wall(350, 50, 10, 100, Colors.color_white))
        list_of_walls.append(Wall(50, 500, 500, 10, Colors.color_white))

        # Bananer:
        list_of_bananas = pygame.sprite.Group()
        list_of_bananas.add(Banana(70, 100))
        list_of_bananas.add(Banana(200, 90))
        list_of_bananas.add(Banana(400, 400))
        list_of_bananas.add(Banana(600, 400))
        list_of_bananas.add(Banana(72, 20))

        # Snake:
        sprite_slange = Snake(200, 200, Snake.VENSTRE, 3, Colors.color_white)

        level = Level(list_of_walls, list_of_bananas, 200, 200, sprite_slange)
        level.set_background_color(Colors.color_orange)
        return level

    def get_level_2(self):
        # Walls:
        list_of_walls = []
        list_of_walls.append(Wall(50, 50, 40, 10, Colors.color_white))
        list_of_walls.append(Wall(350, 50, 100, 10, Colors.color_white))
        list_of_walls.append(Wall(150, 500, 500, 10, Colors.color_white))

        # Bananer:
        list_of_bananas = pygame.sprite.Group()
        banana_1 = Banana(70, 100)
        list_of_bananas.add(banana_1)

        # Snake:
        sprite_slange = Snake(200, 200, Snake.NED, 3, Colors.color_white)

        level = Level(list_of_walls, list_of_bananas, 200, 200, sprite_slange)
        level.set_background_color(Colors.color_pink)
        return level

