
class Colors:

    color_green = (0, 255, 0)
    color_red = (255, 0, 0)
    color_black = (0, 0, 0)
    color_orange = (255, 128, 0)
    color_white = (255, 255, 255)
    color_pink = (255, 0, 255)
