import pygame


class Banana(pygame.sprite.Sprite):

    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('../images/Banana_small.png')
        self.rect = pygame.Rect(x, y, 25, 21)

