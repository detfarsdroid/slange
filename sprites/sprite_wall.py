from pygame.rect import Rect


class Wall:

    def __init__(self, x, y, whit, height, color):
        self.x = x
        self.y = y
        self.whit = whit
        self.height = height
        self.color = color

    def get_rect(self):
        return self.x, self.y, self.whit, self.height

    def get_color(self):
        return self.color

