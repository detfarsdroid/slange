class Snake:
    OP = "OP"
    NED = "NED"
    VENSTRE = "VENSTRE"
    HØJRE = "HØJRET"

    snake_joins = []
    snake_length = 5
    snake_body_size = 10

    def __init__(self, x, y, start_direction, move_number_of_pixel, color):
        self.current_x = x
        self.current_y = y
        self.start_direction = start_direction
        self.antal_pixels_ormen_flytter_sig = move_number_of_pixel
        self.color = color

    def set_snake_direction(self, direction):
        if direction == self.OP:
            self.current_y = self.current_y - self.antal_pixels_ormen_flytter_sig
        if direction == self.NED:
            self.current_y = self.current_y + self.antal_pixels_ormen_flytter_sig
        if direction == self.VENSTRE:
            self.current_x = self.current_x - self.antal_pixels_ormen_flytter_sig
        if direction == self.HØJRE:
            self.current_x = self.current_x + self.antal_pixels_ormen_flytter_sig

        self.snake_moved_to(self.current_x, self.current_y)

    def snake_moved_to(self, x, y):
        # We add the new coordinate to the first place in the list "snake_joints"
        self.snake_joins.insert(0, (x, y, self.snake_body_size, self.snake_body_size))
        if len(self.snake_joins) > self.snake_length:
            self.snake_joins.pop()

    def increase_snake_length(self):
        self.snake_length = self.snake_length + 10

    def get_rect_to_draw(self):
        return self.snake_joins


